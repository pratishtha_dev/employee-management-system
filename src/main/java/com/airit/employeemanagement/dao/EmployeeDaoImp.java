package com.airit.employeemanagement.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.airit.employeemanagement.exception.ResourceNotFoundException;
import com.airit.employeemanagement.model.Employee;

@Repository
public class EmployeeDaoImp implements EmployeeDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public long save(Employee employee) {
		sessionFactory.getCurrentSession().save(employee);
		return employee.getId();
	}

	public Employee get(long id) throws ResourceNotFoundException {
		Session session = sessionFactory.getCurrentSession();
		String getHql = "FROM employee WHERE id = :id";
		Query query = session.createQuery(getHql);
		query.setParameter("id", id);
		List results = query.list();
		if (results.size() <= 0)
			throw new ResourceNotFoundException("The Requested employee doesnt exist!");
		return (Employee) results.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<Employee> list() {
		return sessionFactory.getCurrentSession().createQuery("from employee").list();
	}

	public void update(long id, Employee employee) throws ResourceNotFoundException {
		Session session = sessionFactory.getCurrentSession();
		Employee dbEmployee = get(id);
		if (dbEmployee == null)
			throw new ResourceNotFoundException("The Requested employee doesnt exist!");
		String updateHql = "update employee set name=:name, address=:address, department=:department, salary=:salary where id=:id";
		Query query = session.createQuery(updateHql);
		query.setParameter("name", employee.getName());
		query.setParameter("address", employee.getAddress());
		query.setParameter("department", employee.getDepartment());
		query.setParameter("salary", employee.getSalary());
		query.setParameter("id", id);
		query.executeUpdate();
		session.flush();
	}

	@Override
	public void delete(long id) throws ResourceNotFoundException {
		Session session = sessionFactory.getCurrentSession();
		String deleteHql = "delete from employee where id= :id";
		Query query = session.createQuery(deleteHql);
		query.setParameter("id", id);
		query.executeUpdate();
	}

}
