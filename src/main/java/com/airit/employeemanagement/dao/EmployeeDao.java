package com.airit.employeemanagement.dao;

import java.util.List;

import com.airit.employeemanagement.exception.ResourceNotFoundException;
import com.airit.employeemanagement.model.Employee;

public interface EmployeeDao {
	long save(Employee employee);

	Employee get(long id) throws ResourceNotFoundException;

	List<Employee> list();

	void update(long id, Employee employee) throws ResourceNotFoundException;

	void delete(long id) throws ResourceNotFoundException;
}
