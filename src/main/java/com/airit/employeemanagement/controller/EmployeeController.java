package com.airit.employeemanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.airit.employeemanagement.exception.ResourceNotFoundException;
import com.airit.employeemanagement.model.Employee;
import com.airit.employeemanagement.service.EmployeeService;

@RestController
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;

	@PostMapping("/employee")
	public ResponseEntity<?> save(@RequestBody Employee employee) {
		long id = employeeService.save(employee);
		return ResponseEntity.ok().body("New Employee has been saved with ID:" + id);
	}

	@GetMapping("/employee/{id}")
	public ResponseEntity<Employee> get(@PathVariable("id") long id) throws ResourceNotFoundException {
		Employee employee = employeeService.get(id);
		return ResponseEntity.ok().body(employee);
	}

	@GetMapping("/employee")
	public ResponseEntity<List<Employee>> list() {
		List<Employee> employees = employeeService.list();
		return ResponseEntity.ok().body(employees);
	}

	@PutMapping("/employee/{id}")
	public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody Employee employee) throws ResourceNotFoundException {
		employeeService.update(id, employee);
		return ResponseEntity.ok().body("Employee has been updated successfully.");
	}

	@DeleteMapping("/employee/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") long id) throws ResourceNotFoundException {
		employeeService.delete(id);
		return ResponseEntity.ok().body("Employee has been deleted successfully.");
	}

}
