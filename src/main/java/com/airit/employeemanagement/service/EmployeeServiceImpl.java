package com.airit.employeemanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.airit.employeemanagement.dao.EmployeeDao;
import com.airit.employeemanagement.exception.ResourceNotFoundException;
import com.airit.employeemanagement.model.Employee;

@Service
@Transactional(readOnly = true)
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	private EmployeeDao employeeDao;

	@Transactional
	@Override
	public long save(Employee employee) {
		return employeeDao.save(employee);
	}

	@Override
	public Employee get(long id) throws ResourceNotFoundException {
		return employeeDao.get(id);
	}

	@Override
	public List<Employee> list() {
		return employeeDao.list();
	}

	@Transactional
	@Override
	public void update(long id, Employee employee) throws ResourceNotFoundException {
		employeeDao.update(id, employee);
	}

	@Transactional
	@Override
	public void delete(long id) throws ResourceNotFoundException {
		employeeDao.delete(id);
	}
}
